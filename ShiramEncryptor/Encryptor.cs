﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ShiramEncryptor
{
    public class Encryptor
    {
        private string PassPhrase = "The+gOoD-oLdxDai2*wErE2g0oD/LoV3wEre2g0oD";

        private readonly byte[] IV =
        {
            0X01, 0X02, 0X03, 0X04, 0X05, 0X06, 0X07, 0X08,
            0X09, 0X0A, 0X0B, 0X0C, 0X0D, 0X0E, 0X0F, 0X10
        };

        private byte[] DeriveKeyFromPassword()
        {
            var EmptySalt = Array.Empty<byte>();
            var HashMethod = HashAlgorithmName.SHA384;
            using (var rfc2898DeriveBytes = new Rfc2898DeriveBytes(Encoding.Unicode.GetBytes(PassPhrase), EmptySalt, 1000, HashMethod))
            {
                return rfc2898DeriveBytes.GetBytes(32);
            }
        }

        /// <summary>
        /// Encrypts the specified clear text using AES encryption algorithm.
        /// </summary>
        /// <param name="ClearText">The clear text to encrypt.</param>
        /// <returns>The encrypted cipher text.</returns>
        public async Task<byte[]> ShiramEncryptAsync(string ClearText)
        {
            Aes aes = Aes.Create();
            aes.Key = DeriveKeyFromPassword();
            aes.IV = IV;

            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, aes.CreateEncryptor(), CryptoStreamMode.Write);
            await cryptoStream.WriteAsync(Encoding.Unicode.GetBytes(ClearText), 0, Encoding.Unicode.GetBytes(ClearText).Length);
            await cryptoStream.FlushAsync();

            return memoryStream.ToArray();
        }

        /// <summary>
        /// Decrypts the specified cipher text using AES encryption algorithm.
        /// </summary>
        /// <param name="CipherText">The cipher text to decrypt.</param>
        /// <returns>The decrypted clear text.</returns>
        public async Task<string> ShiramDecryptAsync(byte[] CipherText)
        {
            Aes aes = Aes.Create();
            aes.Key = DeriveKeyFromPassword();
            aes.IV = IV;

            MemoryStream memoryStream = new MemoryStream(CipherText);
            CryptoStream cryptoStream = new CryptoStream(memoryStream, aes.CreateDecryptor(), CryptoStreamMode.Read);

            MemoryStream output = new MemoryStream();
            await cryptoStream.CopyToAsync(output);

            return Encoding.Unicode.GetString(output.ToArray());
        }
    }
}